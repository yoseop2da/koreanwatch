//
//  main.m
//  NumberWatch
//
//  Created by parkyoseop on 2015. 12. 21..
//  Copyright © 2015년 yoseop2da. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
