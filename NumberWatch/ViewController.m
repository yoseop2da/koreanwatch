//
//  ViewController.m
//  NumberWatch
//
//  Created by parkyoseop on 2015. 12. 21..
//  Copyright © 2015년 yoseop2da. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>

#define DEFAULT_COLOR [UIColor colorWithRed:197/255.f green:192/255.f blue:162/255.f alpha:1.f]
#define SELECTED_COLOR [UIColor colorWithRed:255/255.f green:149/255.f blue:13/255.f alpha:1.f]
#define DEFAULT_FONT(s) [UIFont fontWithName:@"AppleSDGothicNeo-Thin" size:s]
#define SELECTED_FONT(s) [UIFont fontWithName:@"AppleSDGothicNeo-SemiBold" size:s]
#define SELECTED_AMPM_FONT(s) [UIFont fontWithName:@"AppleSDGothicNeo-SemiBold" size:s]

@interface ViewController ()
{
    NSString *_hourString;
    NSString *_minuteString;
    NSString *_secondString;
    NSString *_ampmText;
    IBOutlet UILabel *amLabel;
    IBOutlet UILabel *pmLabel;
    
    IBOutletCollection(UILabel) NSArray *hourFirstLabels;
    IBOutletCollection(UILabel) NSArray *hourSecondLabelsOneToFive;
    IBOutletCollection(UILabel) NSArray *hourSecondLabelsSixToZero;
    __weak IBOutlet UIView *hourSixToZero;
    __weak IBOutlet UIView *hourOneToFive;
    
    IBOutletCollection(UILabel) NSArray *minFirstLabels;
    IBOutletCollection(UILabel) NSArray *minSecondLabelsOneToFive;
    IBOutletCollection(UILabel) NSArray *minSecondLabelsSixToZero;
    __weak IBOutlet UIView *minSixToZero;
    __weak IBOutlet UIView *minOneToFive;
    
    IBOutletCollection(UILabel) NSArray *secFirstLabels;
    IBOutletCollection(UILabel) NSArray *secSecondLabelsOneToFive;
    IBOutletCollection(UILabel) NSArray *secSecondLabelsSixToZero;
    __weak IBOutlet UIView *secSixToZero;
    __weak IBOutlet UIView *secOneToFive;
    
    __weak IBOutlet UIView *settingView;
    
    __weak IBOutlet UIView *settingInnerView;
    BOOL _isSoundOn;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(getTime) userInfo:nil repeats:YES];
    [self getTime];

    settingInnerView.layer.cornerRadius = 20.f;
    settingInnerView.clipsToBounds = YES;
    
    _isSoundOn = [[NSUserDefaults standardUserDefaults] boolForKey:@"IS_SOUND_ON"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)switchButtonTouched:(UISwitch *)sender
{
    _isSoundOn = sender.on;
    [[NSUserDefaults standardUserDefaults] setBool:_isSoundOn forKey:@"IS_SOUND_ON"];
}

- (IBAction)dimButtonTouched:(id)sender
{
    settingView.hidden = !settingView.hidden;
}

- (IBAction)soundSettingButtonTouched:(id)sender
{
    settingView.hidden = !settingView.hidden;
}

- (void)getTime
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"hh:mm:ss:HH";
    NSString *timeString = [dateFormatter stringFromDate:[NSDate date]];
    NSArray *arr = [timeString componentsSeparatedByString:@":"];
          
    _hourString = arr[0];
    _minuteString = arr[1];
    _secondString = arr[2];
    
    if (([arr[3] intValue] - 12) > 0) {
        _ampmText = @"오후";
        [self selectedAMPMLabel:pmLabel];
        [self defaultLabel:amLabel];
    }else{
        _ampmText = @"오전";
        [self selectedAMPMLabel:amLabel];
        [self defaultLabel:pmLabel];
    }
    
    if (_isSoundOn) {
        NSString *ttsString = [NSString stringWithFormat:@"%@ %@시 %@분 %@초",_ampmText, _hourString,_minuteString,_secondString];
        NSLog(@"%@",ttsString);
        AVSpeechSynthesizer *synthesizer = [[AVSpeechSynthesizer alloc]init];
        AVSpeechUtterance *utterance = [AVSpeechUtterance speechUtteranceWithString:ttsString];
        [utterance setRate:.4f];
        utterance.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"ko-KR"];
        
        [synthesizer speakUtterance:utterance];
    }
    
    [self displayTime];
}

- (void)displayTime
{
    [self showHour];
    [self showMin];
    [self showSec];
}

- (void)showHour
{
    int first = [[_hourString substringToIndex:1] intValue];
    int second = [[_hourString substringFromIndex:1] intValue];
    
    if (first == 0) {
        [self hideHourSecond:NO];
        
        if (second >= 1 && second <= 5 ) {
            for (UILabel *label in hourSecondLabelsSixToZero) {
                
                [self defaultLabel:label];
            }
            
            for (UILabel *label in hourFirstLabels) {
                label.adjustsFontSizeToFitWidth = YES;
                if(label.tag == second){
                    [self selectedLabel:label];
                }else{
                    [self defaultLabel:label];
                }
            }
        }else{
            for (UILabel *label in hourFirstLabels) {
                label.adjustsFontSizeToFitWidth = YES;
                [self defaultLabel:label];
                
                switch (label.tag) {
                    case 1:
                        label.text = @"한";
                        break;
                    case 2:
                        label.text = @"두";
                        break;
                    case 3:
                        label.text = @"세";
                        break;
                    case 4:
                        label.text = @"네";
                        break;
                    case 5:
                        label.text = @"다섯";
                        break;
                        
                    default:
                        break;
                }
            }
            for (UILabel *label in hourSecondLabelsSixToZero) {
                if(label.tag == second){
                    if (second == 0) {
                        [self defaultLabel:label];
                    }else{
                        [self selectedLabel:label];
                    }
                }else{
                    [self defaultLabel:label];
                }
            }
        }
    }else{
        if(second == 0) {
            for (UILabel *label in hourFirstLabels) {
                [self defaultLabel:label];
            }
            
            [self hideHourSecond:NO];
            for (UILabel *label in hourSecondLabelsOneToFive) {
                [self defaultLabel:label];
                label.adjustsFontSizeToFitWidth = YES;
                switch (label.tag) {
                    case 1:
                        label.text = @"한";
                        break;
                    case 2:
                        label.text = @"두";
                        break;
                    case 3:
                        label.text = @"세";
                        break;
                    case 4:
                        label.text = @"네";
                        break;
                    case 5:
                        label.text = @"다섯";
                        break;
                        
                    default:
                        break;
                }
            }
            
            for (UILabel *label in hourSecondLabelsSixToZero) {
                if(label.tag == second){
                    [self selectedLabel:label];
                }else{
                    [self defaultLabel:label];
                }
            }
        }else{
            // First 표시
            for (UILabel *label in hourFirstLabels) {
                label.adjustsFontSizeToFitWidth = YES;
                
                switch (label.tag) {
                    case 1:
                        label.text = @"열";
                        break;
                    case 2:
                        label.text = @"두";
                        break;
                    case 3:
                        label.text = @"세";
                        break;
                    case 4:
                        label.text = @"네";
                        break;
                    case 5:
                        label.text = @"다섯";
                        break;
                        
                    default:
                        break;
                }
                if(label.tag == first){
                    [self selectedLabel:label];
                    switch (label.tag) {
                        case 1:
                            label.text = @"열";
                            break;
                        case 2:
                            label.text = @"두";
                            break;
                        case 3:
                            label.text = @"세";
                            break;
                        case 4:
                            label.text = @"네";
                            break;
                        case 5:
                            label.text = @"다섯";
                            break;
                            
                        default:
                            break;
                    }
                }else{
                    [self defaultLabel:label];
                }
            }
            
            // Second 표시
            if (second >= 1 && second <= 5 ) {
                [self hideHourSecond:YES];
                for (UILabel *label in hourSecondLabelsSixToZero) {
                    [self defaultLabel:label];
                }
                
                for (UILabel *label in hourSecondLabelsOneToFive) {
                    if(label.tag == second){
                        [self selectedLabel:label];
                    }else{
                        [self defaultLabel:label];
                    }
                }
            }else{
                [self hideHourSecond:NO];
                for (UILabel *label in hourSecondLabelsOneToFive) {
                    [self defaultLabel:label];
                    label.adjustsFontSizeToFitWidth = YES;
                    
                    switch (label.tag) {
                        case 1:
                            label.text = @"한";
                            break;
                        case 2:
                            label.text = @"두";
                            break;
                        case 3:
                            label.text = @"세";
                            break;
                        case 4:
                            label.text = @"네";
                            break;
                        case 5:
                            label.text = @"다섯";
                            break;
                            
                        default:
                            break;
                    }
                }
                
                for (UILabel *label in hourSecondLabelsSixToZero) {
                    if(label.tag == second){
                        [self selectedLabel:label];
                    }else{
                        [self defaultLabel:label];
                    }
                }
            }
        }
        
    }
    
//    if (first == 0) {
//        [self hideHourSecond:NO];
//        if (second >= 1 && second <= 5 ) {
//            for (UILabel *label in hourSecondLabelsSixToZero) {
//                [self defaultLabel:label];
//            }
//
//            for (UILabel *label in hourFirstLabels) {
//                switch (label.tag) {
//                    case 1:
//                        label.text = @"한";
//                        break;
//                    case 2:
//                        label.text = @"두";
//                        break;
//                    case 3:
//                        label.text = @"세";
//                        break;
//                    case 4:
//                        label.text = @"네";
//                        break;
//                    case 5:
//                        label.text = @"다섯";
//                        break;
//                        
//                    default:
//                        break;
//                }
//                if(label.tag == second){
//                    [self selectedLabel:label];
//                }else{
//                    [self defaultLabel:label];
//                }
//            }
//        }else{
//            for (UILabel *label in hourFirstLabels) {
//                switch (label.tag) {
//                    case 1:
//                        label.text = @"한";
//                        break;
//                    case 2:
//                        label.text = @"두";
//                        break;
//                    case 3:
//                        label.text = @"세";
//                        break;
//                    case 4:
//                        label.text = @"네";
//                        break;
//                    case 5:
//                        label.text = @"다섯";
//                        break;
//                        
//                    default:
//                        break;
//                }
//
//                [self defaultLabel:label];
//            }
//            
//            for (UILabel *label in hourSecondLabelsSixToZero) {
//                if(label.tag == second){
//                    [self selectedLabel:label];
//                }else{
//                    [self defaultLabel:label];
//                }
//            }
//        }
//    }else{
//        // 10, 11, 12,
//        // First 표시
//        for (UILabel *label in hourFirstLabels) {
//            switch (label.tag) {
//                case 1:
//                    label.text = @"열";
//                    break;
//                case 2:
//                    label.text = @"두";
//                    break;
//                case 3:
//                    label.text = @"세";
//                    break;
//                case 4:
//                    label.text = @"네";
//                    break;
//                case 5:
//                    label.text = @"다섯";
//                    break;
//                    
//                default:
//                    break;
//            }
//
//            if(label.tag == first){
//                [self selectedLabel:label];
//            }else{
//                [self defaultLabel:label];
//            }
//        }
//        
//        // Second 표시
//        if (second >= 1 && second <= 5 ) {
//            [self hideHourSecond:YES];
//            for (UILabel *label in hourSecondLabelsSixToZero) {
//                [self defaultLabel:label];
//            }
//            
//            for (UILabel *label in hourSecondLabelsOneToFive) {
//                if(label.tag == second){
//                    [self selectedLabel:label];
//                }else{
//                    [self defaultLabel:label];
//                }
//            }
//        }else{
//            [self hideHourSecond:NO];
//            for (UILabel *label in hourSecondLabelsOneToFive) {
//                [self defaultLabel:label];
//            }
//            
//            for (UILabel *label in hourSecondLabelsSixToZero) {
//                if(label.tag == second){
//                    [self selectedLabel:label];
//                }else{
//                    [self defaultLabel:label];
//                }
//            }
//        }
//    }
    
}

- (void)showMin
{
    int first = [[_minuteString substringToIndex:1] intValue];
    int second = [[_minuteString substringFromIndex:1] intValue];
    
    if (first == 0) {
        [self hideMinSecond:NO];
        
        if (second >= 1 && second <= 5 ) {
            for (UILabel *label in minSecondLabelsSixToZero) {
                
                [self defaultLabel:label];
            }
            
            for (UILabel *label in minFirstLabels) {
                label.adjustsFontSizeToFitWidth = YES;
                if(label.tag == second){
                    [self selectedLabel:label];
                }else{
                    [self defaultLabel:label];
                }
            }
        }else{
            for (UILabel *label in minFirstLabels) {
                label.adjustsFontSizeToFitWidth = YES;
                [self defaultLabel:label];
                
                switch (label.tag) {
                    case 1:
                        label.text = @"일";
                        break;
                    case 2:
                        label.text = @"이";
                        break;
                    case 3:
                        label.text = @"삼";
                        break;
                    case 4:
                        label.text = @"사";
                        break;
                    case 5:
                        label.text = @"오";
                        break;
                    default:
                        break;
                }
            }
            for (UILabel *label in minSecondLabelsSixToZero) {
                if(label.tag == second){
                    if (second == 0) {
                        [self defaultLabel:label];
                    }else{
                        [self selectedLabel:label];
                    }
                }else{
                    [self defaultLabel:label];
                }
            }
        }
    }else{
        if(second == 0) {
            for (UILabel *label in minFirstLabels) {
                label.adjustsFontSizeToFitWidth = YES;
                if(label.tag == first){
                    [self selectedLabel:label];
                }else{
                    [self defaultLabel:label];
                }
            }
            
            [self hideMinSecond:NO];
            for (UILabel *label in minSecondLabelsOneToFive) {
                [self defaultLabel:label];
                label.adjustsFontSizeToFitWidth = YES;
                switch (label.tag) {
                    case 1:
                        label.text = @"일";
                        break;
                    case 2:
                        label.text = @"이";
                        break;
                    case 3:
                        label.text = @"삼";
                        break;
                    case 4:
                        label.text = @"사";
                        break;
                    case 5:
                        label.text = @"오";
                        break;
                    default:
                        break;
                }
            }
            
            for (UILabel *label in minSecondLabelsSixToZero) {
                if(label.tag == second){
                    [self selectedLabel:label];
                }else{
                    [self defaultLabel:label];
                }
            }
        }else{
            // First 표시
            for (UILabel *label in minFirstLabels) {
                label.adjustsFontSizeToFitWidth = YES;
                
                switch (label.tag) {
                    case 1:
                        label.text = @"십";
                        break;
                    case 2:
                        label.text = @"이";
                        break;
                    case 3:
                        label.text = @"삼";
                        break;
                    case 4:
                        label.text = @"사";
                        break;
                    case 5:
                        label.text = @"오";
                        break;
                    default:
                        break;
                }
                
                if(label.tag == first){
                    [self selectedLabel:label];
                    switch (label.tag) {
                        case 1:
                            label.text = @"십";
                            break;
                        case 2:
                            label.text = @"이십";
                            break;
                        case 3:
                            label.text = @"삼십";
                            break;
                        case 4:
                            label.text = @"사십";
                            break;
                        case 5:
                            label.text = @"오십";
                            break;
                        default:
                            break;
                    }
                }else{
                    [self defaultLabel:label];
                }
            }
            
            // Second 표시
            if (second >= 1 && second <= 5 ) {
                [self hideMinSecond:YES];
                for (UILabel *label in minSecondLabelsSixToZero) {
                    [self defaultLabel:label];
                }
                
                for (UILabel *label in minSecondLabelsOneToFive) {
                    if(label.tag == second){
                        [self selectedLabel:label];
                    }else{
                        [self defaultLabel:label];
                    }
                }
            }else{
                [self hideMinSecond:NO];
                for (UILabel *label in minSecondLabelsOneToFive) {
                    [self defaultLabel:label];
                    label.adjustsFontSizeToFitWidth = YES;
                    
                    switch (label.tag) {
                        case 1:
                            label.text = @"일";
                            break;
                        case 2:
                            label.text = @"이";
                            break;
                        case 3:
                            label.text = @"삼";
                            break;
                        case 4:
                            label.text = @"사";
                            break;
                        case 5:
                            label.text = @"오";
                            break;
                        default:
                            break;
                    }
                }
                
                for (UILabel *label in minSecondLabelsSixToZero) {
                    if(label.tag == second){
                        [self selectedLabel:label];
                    }else{
                        [self defaultLabel:label];
                    }
                }
            }
        }
        
    }
}

- (void)showSec
{
    int first = [[_secondString substringToIndex:1] intValue];
    int second = [[_secondString substringFromIndex:1] intValue];
    
    if (first == 0) {
        [self hideSecSecond:NO];

        if (second >= 1 && second <= 5 ) {
            for (UILabel *label in secSecondLabelsSixToZero) {
                
                [self defaultLabel:label];
            }
            
            for (UILabel *label in secFirstLabels) {
                label.adjustsFontSizeToFitWidth = YES;
                if(label.tag == second){
                    [self selectedLabel:label];
                }else{
                    [self defaultLabel:label];
                }
            }
        }else{
            for (UILabel *label in secFirstLabels) {
                label.adjustsFontSizeToFitWidth = YES;
                [self defaultLabel:label];
                
                switch (label.tag) {
                    case 1:
                        label.text = @"일";
                        break;
                    case 2:
                        label.text = @"이";
                        break;
                    case 3:
                        label.text = @"삼";
                        break;
                    case 4:
                        label.text = @"사";
                        break;
                    case 5:
                        label.text = @"오";
                        break;
                    default:
                        break;
                }
            }
            for (UILabel *label in secSecondLabelsSixToZero) {
                if(label.tag == second){
                    if (second == 0) {
                        [self defaultLabel:label];
                    }else{
                        [self selectedLabel:label];
                    }
                }else{
                    [self defaultLabel:label];
                }
            }
        }
    }else{
        if(second == 0) {
            for (UILabel *label in secFirstLabels) {
                label.adjustsFontSizeToFitWidth = YES;
                if(label.tag == first){
                    [self selectedLabel:label];
                }else{
                    [self defaultLabel:label];
                }
            }
            
            [self hideSecSecond:NO];
            for (UILabel *label in secSecondLabelsOneToFive) {
                [self defaultLabel:label];
                label.adjustsFontSizeToFitWidth = YES;
                switch (label.tag) {
                    case 1:
                        label.text = @"일";
                        break;
                    case 2:
                        label.text = @"이";
                        break;
                    case 3:
                        label.text = @"삼";
                        break;
                    case 4:
                        label.text = @"사";
                        break;
                    case 5:
                        label.text = @"오";
                        break;
                    default:
                        break;
                }
            }
            
            for (UILabel *label in secSecondLabelsSixToZero) {
                if(label.tag == second){
                    [self selectedLabel:label];
                }else{
                    [self defaultLabel:label];
                }
            }
        }else{
            // First 표시
            for (UILabel *label in secFirstLabels) {
                label.adjustsFontSizeToFitWidth = YES;
                
                switch (label.tag) {
                    case 1:
                        label.text = @"십";
                        break;
                    case 2:
                        label.text = @"이";
                        break;
                    case 3:
                        label.text = @"삼";
                        break;
                    case 4:
                        label.text = @"사";
                        break;
                    case 5:
                        label.text = @"오";
                        break;
                    default:
                        break;
                }
                
                if(label.tag == first){
                    [self selectedLabel:label];
                    switch (label.tag) {
                        case 1:
                            label.text = @"십";
                            break;
                        case 2:
                            label.text = @"이십";
                            break;
                        case 3:
                            label.text = @"삼십";
                            break;
                        case 4:
                            label.text = @"사십";
                            break;
                        case 5:
                            label.text = @"오십";
                            break;
                        default:
                            break;
                    }
                }else{
                    [self defaultLabel:label];
                }
            }
            
            // Second 표시
            if (second >= 1 && second <= 5 ) {
                [self hideSecSecond:YES];
                for (UILabel *label in secSecondLabelsSixToZero) {
                    [self defaultLabel:label];
                }
                
                for (UILabel *label in secSecondLabelsOneToFive) {
                    if(label.tag == second){
                        [self selectedLabel:label];
                    }else{
                        [self defaultLabel:label];
                    }
                }
            }else{
                [self hideSecSecond:NO];
                for (UILabel *label in secSecondLabelsOneToFive) {
                    [self defaultLabel:label];
                    label.adjustsFontSizeToFitWidth = YES;
                    
                    switch (label.tag) {
                        case 1:
                            label.text = @"일";
                            break;
                        case 2:
                            label.text = @"이";
                            break;
                        case 3:
                            label.text = @"삼";
                            break;
                        case 4:
                            label.text = @"사";
                            break;
                        case 5:
                            label.text = @"오";
                            break;
                        default:
                            break;
                    }
                }
                
                for (UILabel *label in secSecondLabelsSixToZero) {
                    if(label.tag == second){
                        [self selectedLabel:label];
                    }else{
                        [self defaultLabel:label];
                    }
                }
            }
        }

    }
}

- (void)selectedAMPMLabel:(UILabel *)label
{
    label.textColor = SELECTED_COLOR;
    label.font = SELECTED_AMPM_FONT(label.font.pointSize);
}

- (void)selectedLabel:(UILabel *)label
{
    label.textColor = SELECTED_COLOR;
    label.font = SELECTED_FONT(label.font.pointSize);
}

- (void)defaultLabel:(UILabel *)label
{
    label.textColor = DEFAULT_COLOR;
    label.font = DEFAULT_FONT(label.font.pointSize);
}

- (void)hideHourSecond:(BOOL)isHidden
{
    [UIView animateWithDuration:0.5f animations:^{
        [self.view layoutIfNeeded];
        if (isHidden) {
            hourSixToZero.alpha = 0.f;
            hourOneToFive.alpha = 1.f;
        }else{
            hourSixToZero.alpha = 1.f;
            hourOneToFive.alpha = 0.f;
        }
        
    }];
}


- (void)hideMinSecond:(BOOL)isHidden
{
    [UIView animateWithDuration:0.5f animations:^{
        [self.view layoutIfNeeded];
        if (isHidden) {
            minSixToZero.alpha = 0.f;
            minOneToFive.alpha = 1.f;
        }else{
            minSixToZero.alpha = 1.f;
            minOneToFive.alpha = 0.f;
        }
        
    }];
}


- (void)hideSecSecond:(BOOL)isHidden
{
    [UIView animateWithDuration:0.5f animations:^{
        [self.view layoutIfNeeded];
        if (isHidden) {
            secSixToZero.alpha = 0.f;
            secOneToFive.alpha = 1.f;
        }else{
            secSixToZero.alpha = 1.f;
            secOneToFive.alpha = 0.f;
        }
        
    }];
}

@end
